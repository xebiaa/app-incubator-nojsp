package com.xebia.nojsp

import collection.JavaConversions._
import com.mongodb.{DBObject, Mongo, BasicDBObject}

/**
 * @author Iwein Fuld
 */

object MongoRepository {
  val m = new Mongo();
  val db = m.getDB("nojsp");
  val coll = db.getCollection("projects");

  def getProject(projectnr: String): String = {
    val query = new BasicDBObject();
    query.put("project", projectnr);
    val cur = coll.find(query);
    cur.next.toString;
  }

  def getProjects() : String = {
    val cur = coll.find();
    cur.toArray.mkString(" ")
  }
}