package com.xebia.nojsp

import org.scalatra.ScalatraFilter;

import net.liftweb.mongodb._

import com.mongodb.DBCollection;
import com.mongodb.Mongo;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.DBCursor;

class NoJspService extends ScalatraFilter {

	before {
		contentType = "application/json"
	}

	get("/json/projects") {
		//"""{ projects: ["project/1", "project/2"] }"""
    """{ "projects": [ "project/1", "project/2", "project/3"] }"""
    MongoRepository.getProjects
	}
	
	get("/json/project/:projectnr") {
    MongoRepository.getProject(params("projectnr"))
	}
	
	get("/json/item/:itemnr") {
		"""{ 	project: "project/1",                
							title: "Title of item 1",
							description: "Description of item",
							image: "img/image1_1.png"
							}"""
	}
	
	get("/json/ping") {
		
		MongoDB.defineDb(DefaultMongoIdentifier, MongoAddress(MongoHost("localhost", 27017), "test"))
		
		MongoDB.use(DefaultMongoIdentifier) ( db => {
		 	val doc = new BasicDBObject
		  
			doc.put("name", "MongoDB")
		 	doc.put("type", "database")
		 	doc.put("count", 1)
		
		  	val coll = db.getCollection("projects")

		  	coll.save(doc)
		})
		
		""" { "result" : "OK"} """
	}
}
