var init = function() {
	$.getJSON("json/projects", renderList);
	renderList(projects);
	attachEvents();
}

var listClick = function(event) {
	var url=event.target.getAttribute("href");
	$.getJSON("json/" + url, renderProject);
}

var renderList = function(listData) {
	$('div#list').render(listData, listTemplate);
	$.getJSON("json/" + listData.projects[0], renderProject);
	attachEvents();
}

var renderProject = function(projectData) {
	$('div#prj').render(projectData, projectTemplate);
	$.getJSON("json/" + projectData.items[0], renderItem);
}

var renderItem = function(itemData) {
	$('div#item').render(itemData, itemTemplate);
}

var projectClick = function(event) {
	var url=event.target.getAttribute("href");
	renderItemByUrl(url);
	attachEvents();
}

function attachEvents() {
	$('div#list h4').click(listClick);
	$('div#prj h4').click(projectClick);
}
