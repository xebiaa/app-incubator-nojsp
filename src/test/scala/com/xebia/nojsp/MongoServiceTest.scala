package com.xebia.nojsp

import org.scalatest.junit.JUnitSuite
import org.scalatest.FlatSpec
import org.scalatest.matchers.ShouldMatchers
import org.junit.runner.RunWith
import org.junit.Test
import org.scalatest.junit.JUnitRunner

/**
 * @author Iwein Fuld
 */
@RunWith(classOf[JUnitRunner])
class MongoRepositorySpec extends FlatSpec with ShouldMatchers {
  "A Repository " should "return all projects " in {
    MongoRepository.getProjects should include ("{")
  }

  "A Repository " should "return a single project " in {
    MongoRepository.getProject("1") should include ("{")
  }
}
